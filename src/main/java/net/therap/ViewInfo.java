package net.therap;

import java.util.HashSet;
import java.util.Set;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class ViewInfo {
    public int get;
    public int post;
    public int totalResponseTime;
    public Set<String> uniqueUri;

    public ViewInfo() {
        this.get = 0;
        this.post = 0;
        this.totalResponseTime = 0;
        this.uniqueUri = new HashSet<String>();
    }
}
