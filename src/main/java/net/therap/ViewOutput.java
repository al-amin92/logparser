package net.therap;

import java.util.List;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class ViewOutput {

    private static final int HALF_DAY_HOURS = 12;

    public void view(List<LogInfo> list) {

        System.out.printf("%-18s %-18s %-18s %-20s\n", "Time", "GET / POST Count", "Unique URI Count",
                "Total Response Time");

        for (int i = 0; i < list.size(); i++) {

            int time = list.get(i).time;
            int get = list.get(i).gCount;
            int post = list.get(i).pCount;

            String sTime;
            String getPost = Integer.toString(get) + "/" + Integer.toString(post);
            String responseTime = list.get(i).responseTime + "ms";

            if (time < HALF_DAY_HOURS) {
                if (time == 0) {
                    time = 12;
                }
                sTime = Integer.toString(time) + " am - ";
                if (time == 12) {
                    sTime += "1 am";
                } else if (time == 11) {
                    sTime += "12 pm";
                } else {
                    sTime += Integer.toString(time + 1) + " am";
                }
            } else {
                time -= HALF_DAY_HOURS;
                if (time == 0) {
                    time = 12;
                }
                sTime = Integer.toString(time) + " pm - ";
                if (time == 12) {
                    sTime += "1 pm";
                } else if (time == 11) {
                    sTime += "12 am";
                } else {
                    sTime += Integer.toString(time + 1) + " pm";
                }
            }

            System.out.printf("%-18s %-18s %-18d %-18s\n", sTime, getPost, list.get(i).uri, responseTime);
        }
    }
}
