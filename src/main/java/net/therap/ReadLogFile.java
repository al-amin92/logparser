package net.therap;

import java.io.*;
import java.util.ArrayList;

/**
 * @author al-amin
 * @since 11/7/16
 */

class ReadLogFile {
    public ArrayList<String> getLogs(String fileName) throws IOException {

        ArrayList<String> lines = new ArrayList<>();

        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String str;

        while ((str = bufferedReader.readLine()) != null) {
            lines.add(str);
        }

        return lines;
    }
}
