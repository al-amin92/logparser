package net.therap;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class LogInfo implements Comparable<LogInfo> {

    public int time;
    public int gPCount;
    public int uri;
    public int responseTime;
    public int gCount;
    public int pCount;

    public LogInfo(int time, int gCount, int pCount, int gPCount, int uri, int responseTime) {

        this.time = time;
        this.gCount = gCount;
        this.pCount = pCount;
        this.gPCount = gPCount;
        this.uri = uri;
        this.responseTime = responseTime;
    }

    @Override
    public int compareTo(LogInfo o) {
        return -Integer.compare(gPCount, o.gPCount);
    }


}