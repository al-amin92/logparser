package net.therap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class MatchRegex {

    private String logTime;
    private String getOrPost;
    private String uri;
    private String responseTime;

    public String getLogTime() {
        return logTime;
    }

    public String getGetOrPost() {
        return getOrPost;
    }

    public String getUri() {
        return uri;
    }

    public String getResponseTime() {
        return responseTime;
    }


    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public void setGetOrPost(String getOrPost) {
        this.getOrPost = getOrPost;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    void initStrings() {
        logTime = null;
        getOrPost = null;
        uri = null;
        responseTime = null;
    }

    void match(String line) {

        String pattern = "\\d+-\\d+-\\d+\\s+(\\d+):\\d+:\\d+,\\d+\\s+\\[\\[.*?\\].*?\\]\\s+\\S+\\s+\\S+\\s+\\[.*?\\]" +
                "\\s+\\w+\\s+-\\s+(URI=(\\[.*?\\]),\\s+(G|P),\\s+time=(\\d+)ms.*)|(.*)";

        initStrings();

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line);

        if (m.find()) {
            for (int j = 0; ; j++) {
                if (m.group(j) == null) {
                    break;
                }
                if (j == 1) {
                    setLogTime(m.group(j));
                }
                if (j == 4) {
                    setGetOrPost(m.group(j));
                }
                if (j == 3) {
                    setUri(m.group(j));
                }
                if (j == 5) {
                    setResponseTime(m.group(j));
                }
            }
        } else {
            System.out.println("no match");
        }


    }
}
