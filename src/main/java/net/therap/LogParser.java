package net.therap;

import java.io.*;
import java.util.List;

/**
 * @author al-amin
 * @since 11/7/16
 */


public class LogParser {


    /**
     * Running From Command Line:
     * java LogParser 04-sample-log-file.log
     * java LogParser 04-sample-log-file.log --sort
     * java LogParser
     * Running From IDE:
     * the boolean variable 'sort' determines the output list (true : sorted, false : unsorted)
     */

    public static void main(String[] args) {
        try {

            boolean sort = false;
            Controller controller = new Controller();
            List<LogInfo> list;
            list = controller.generateList(sort, args);

            ViewOutput viewOutput = new ViewOutput();
            viewOutput.view(list);

        } catch (IOException e) {
            System.out.println("File Name Not Specified. Please Try Again.");
        }


    }
}