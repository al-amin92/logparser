package net.therap;

import java.util.*;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class GenerateList {

    public static final int HOURS = 24;
    private List<ViewInfo> viewList = new ArrayList<ViewInfo>();

    public GenerateList() {
        for (int i = 0; i < HOURS; i++) {
            viewList.add(new ViewInfo());
        }
    }

    public void insertInfo(int logTime, char getOrPost, String uri, int responseTime) {
        viewList.get(logTime).uniqueUri.add(uri);
        viewList.get(logTime).totalResponseTime += responseTime;
        if (getOrPost == 'G') {
            viewList.get(logTime).get++;
        } else {
            viewList.get(logTime).post++;
        }
    }

    int getGetCount(int hour) {
        return viewList.get(hour).get;
    }

    int getPostCount(int hour) {
        return viewList.get(hour).post;
    }

    int getUniqueUriCount(int hour) {
        return viewList.get(hour).uniqueUri.size();
    }

    int getTotalResponseTime(int hour) {
        return viewList.get(hour).totalResponseTime;
    }

}

