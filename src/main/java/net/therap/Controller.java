package net.therap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author al-amin
 * @since 11/7/16
 */

public class Controller {

    private static final int HOURS = 24;

    public List<LogInfo> generateList(boolean sort, String[] args) throws IOException {

        ArrayList<String> lines;
        ReadLogFile readLogFile = new ReadLogFile();
        String fileDir;
        fileDir = "/home/al-amin/IdeaProjects/LogParser/src/main/resources/04-sample-log-file.log";

        if (args.length > 0) {
            sort = false;
        }

        if (args.length == 0) {
            lines = readLogFile.getLogs(fileDir);
        } else if (args.length == 1) {
            lines = readLogFile.getLogs(args[0]);
        } else if (args.length == 2 && args[1].equals("--sort")) {
            lines = readLogFile.getLogs(args[0]);
            sort = true;
        } else {
            lines = readLogFile.getLogs(args[0]);
        }

        MatchRegex mRegex = new MatchRegex();
        GenerateList genList = new GenerateList();

        for (int i = 0; i < lines.size(); i++) {
            mRegex.match(lines.get(i));
            if (mRegex.getGetOrPost() == null) {
                continue;
            }
            genList.insertInfo(Integer.parseInt(mRegex.getLogTime()), mRegex.getGetOrPost().charAt(0), mRegex.getUri(),
                    Integer.parseInt(mRegex.getResponseTime()));

        }

        List<LogInfo> list = new ArrayList<>();

        for (int i = 0; i < HOURS; i++) {
            int get = genList.getGetCount(i);
            int post = genList.getPostCount(i);
            list.add(new LogInfo(i, get, post, get + post, genList.getUniqueUriCount(i),
                    genList.getTotalResponseTime(i)));
        }

        if (sort == true) {
            Collections.sort(list);
        }
        return list;
    }

}
